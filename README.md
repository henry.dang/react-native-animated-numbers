# react-native-animated-numbers

![](https://img.shields.io/badge/PRs-Welcome-brightgreen.svg#crop=0&crop=0&crop=1&crop=1&id=ARZjp&originHeight=20&originWidth=92&originalType=binary&ratio=1&rotation=0&showTitle=false&status=done&style=none&title=#crop=0&crop=0&crop=1&crop=1&id=MUXrx&originHeight=20&originWidth=92&originalType=binary&ratio=1&rotation=0&showTitle=false&status=done&style=none&title=)
![](https://img.shields.io/badge/platform-react--native-lightgrey.svg#crop=0&crop=0&crop=1&crop=1&id=fbxM8&originHeight=20&originWidth=134&originalType=binary&ratio=1&rotation=0&showTitle=false&status=done&style=none&title=#crop=0&crop=0&crop=1&crop=1&id=XK0J0&originHeight=20&originWidth=134&originalType=binary&ratio=1&rotation=0&showTitle=false&status=done&style=none&title=)
![](https://img.shields.io/badge/license-MIT-blue.svg#crop=0&crop=0&crop=1&crop=1&id=gORs3&originHeight=20&originWidth=78&originalType=binary&ratio=1&rotation=0&showTitle=false&status=done&style=none&title=#crop=0&crop=0&crop=1&crop=1&id=KyBUH&originHeight=20&originWidth=78&originalType=binary&ratio=1&rotation=0&showTitle=false&status=done&style=none&title=)

[  ](https://www.npmjs.com/package/react-native-animated-numbers)![](http://img.shields.io/npm/v/react-native-animated-numbers.svg?style=flat-square#crop=0&crop=0&crop=1&crop=1&id=QdkPf&originHeight=20&originWidth=80&originalType=binary&ratio=1&rotation=0&showTitle=false&status=done&style=none&title=#crop=0&crop=0&crop=1&crop=1&id=AqaOv&originHeight=20&originWidth=80&originalType=binary&ratio=1&rotation=0&showTitle=false&status=done&style=none&title=)

[  ](https://www.npmjs.com/package/react-native-animated-numbers)![](http://img.shields.io/npm/dm/react-native-animated-numbers.svg?style=flat-square#crop=0&crop=0&crop=1&crop=1&id=e4tjp&originHeight=20&originWidth=144&originalType=binary&ratio=1&rotation=0&showTitle=false&status=done&style=none&title=#crop=0&crop=0&crop=1&crop=1&id=HxHZE&originHeight=20&originWidth=144&originalType=binary&ratio=1&rotation=0&showTitle=false&status=done&style=none&title=)

Library showing animation of number changes in react-native

If you want web version in react.js [download react-animated-numbers](https://github.com/heyman333/react-animated-numbers)

## install

This package is using [react-native-reanimated](https://docs.swmansion.com/react-native-reanimated/docs/about). So following libs should be installed first

```shell
yarn add react-native-reanimated react-native-gesture-handler && cd ios && pod install
```

next

```shell
yarn add react-native-animated-numbers
```

## props
|  | type | default | description |
| --- | --- | --- | --- |
| animateToNumber | number | none | Number to be animated |
| fontStyle | TextStyle? | none | Style of number text |
| animationDuration | number? | 1400(ms) | The speed at which the animation works |
| includeComma | boolean? | false | Whether the number contains commas |
| easing | Easing? | Easing.elastic(1.2) | React Native Easing API in Animated |
| animationDirection | 'clockwise'&#124;'counterclockwise'? | counterclockwise | The direction as number animate from small to large |


## example

```javascript
import React from 'react';
import {SafeAreaView, Button} from 'react-native';
import AnimatedNumbers from 'react-native-animated-numbers';

const App = () => {
  const [animateToNumber, setAnimateToNumber] = React.useState(7979);

  const increase = () => {
    setAnimateToNumber(animateToNumber + 1999);
  };

  return (
    <SafeAreaView
      style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <AnimatedNumbers
        includeComma
        animateToNumber={animateToNumber}
        fontStyle={{fontSize: 50, fontWeight: 'bold'}}
      />
      <Button title="increase" onPress={increase} />
    </SafeAreaView>
  );
};
export default App;
```

## screenshot

![](images/ios.gif#crop=0&crop=0&crop=1&crop=1&id=TQwCS&originalType=binary&ratio=1&rotation=0&showTitle=false&status=done&style=none&title=#crop=0&crop=0&crop=1&crop=1&id=MDI7e&originalType=binary&ratio=1&rotation=0&showTitle=false&status=done&style=none&title=)
![](images/android.gif#crop=0&crop=0&crop=1&crop=1&id=WB9Jr&originalType=binary&ratio=1&rotation=0&showTitle=false&status=done&style=none&title=&width=270#crop=0&crop=0&crop=1&crop=1&id=IfoBW&originalType=binary&ratio=1&rotation=0&showTitle=false&status=done&style=none&title=)
